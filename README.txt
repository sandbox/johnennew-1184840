/**
 * @file
 * README file for multiple true false question type
 */

Quiz multiple true false question type

This allows the quiz creator to ask up to 10 true or false questions on a
single question. This is often more convienient to take, mark and review than
10 questions split over 10 pages.

Sponsored by: College of Emergency Medicine, UK http://www.enlightenme.org
Code: Deeson Group Ltd, http://www.deeson.co.uk


CONTENTS
--------

1.  Installation
2.  Use

----

1. Installation

To install, unpack the module to your modules folder, and simply enable the module at Admin > Build > Modules.
Alternatively you could unpack in the question_types subfolder of the quiz
module if you prefer

----
2. Use

For details on how to use this quiz question type, see the help page once you
have installed it
