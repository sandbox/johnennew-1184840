<?php
// $Id$

/**
 * @file
 * Theme implementations for the Multiple True False Question type
 *
 * Sponsored by the College of Emergency Medicine (UK) http://www.enlightenme.org
 * Code by Deeson Group Ltd http://www.deeson.co.uk
 */

/**
 * Theme the response part of the response report
 *
 * @param $metadata
 *  Can be used as a table header
 * @param $data
 *  Can be used as table rows
 */
function theme_multiple_truefalse_question_response($metadata, $data) {
  $rows = array();
  $p = drupal_get_path('module', 'multiple_truefalse_question') . '/theme/images';
  foreach ($data as $id => $statement) {
    $cols = array();

    $cols[] = array(
      'data' => "<span>{$id}. </span>",
      'width' => 35,
    );

    if ($metadata['showpoints']) {
      $info = $statement['correct']
        ? theme('image', "$p/correct.png", t('Correct'), t('You got this answer correct'), array('class' => 'feedback-icon'))
        : theme('image', "$p/wrong.png",  t('Wrong'), t('You got this answer wrong'), array('class' => 'feedback-icon'));

      $cols[] = array(
        'data' => $info,
        'width' => 35,
        'rowspan' => 1,
        'class' => 'selector-td',
      );
    }

    $cols[] = array(
      'data' => $statement['user_answer']==1 ? t('True') : t('False'),
    );


    if ($metadata['showpoints']) {
      $cols[] = array(
        'data' => $statement['correct_answer']==1 ? t('True') : t('False'),
      );
    }

    $cols[] = array(
      'data' => $statement['statement'],
    );

    if ($metadata['showfeedback']) {
      $cols[] = array(
        'data' => $statement['feedback'],
      );
    }

    $rows[] = $cols;
  }

  $header = array();
  $header[] = array(
    'data' => '',
  );
  if ($metadata['showpoints']) {
    $header[] = array(
      'data' => '',
    );
  }

  $header[] = array(
    'data' => t('Your answer'),
  );

  if ($metadata['showpoints']) {
    $header[] = array(
      'data' => t('Correct Answer'),
    );
  }

  $header[] = array(
    'data' => t('Statement'),
  );

  if ($metadata['showfeedback']) {
    $header[] = array(
      'data' => t('Feedback'),
    );
  }
  return theme('table', $header, $rows);
}
