<?php
/**
 * @file
 * Handles the layout of the multiple_truefalse_question creation form.
 *
 *
 * Variables available:
 * - $form
 */

//krumo($form);

$rows = array();
foreach($form as $id => $target) {
  if (is_numeric($id) && intval($id)>0) {
    $row = array();
    $row[] = drupal_render($target['statement']);
    $row[] = drupal_render($target['answer']);
    $row[] = drupal_render($target['feedback']);
    $rows[] = $row;
  }
}
if (!empty($rows)) {
  $header[] = array('data' => t('Statement'));
  $header[] = array('data' => t('Correct Answer'));
  $header[] = array('data' => t('Feedback'));
  print theme('table', $header, $rows, array(), NULL, TRUE);
}
//print drupal_render($form['feedback']);

// TODO : Add alternatives functionality
//print drupal_render($form['imagetarget_add_alternative']);
