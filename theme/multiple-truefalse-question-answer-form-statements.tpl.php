<?php
/**
 * @file
 * Handles the layout of the multiple_truefalse_question creation form.
 *
 *
 * Variables available:
 * - $form
 */

//krumo($form);

$rows = array();
foreach($form as $id => $statement) {
  if (is_numeric($id) && intval($id)>0) {
    $row = array();
    $row[] = $statement['user_answer']['#title'];
    unset($statement['user_answer']['#title']);
    $row[] = drupal_render($statement['user_answer']);
    $rows[] = $row;
  }
}
if (!empty($rows)) {
  $header[] = array('data' => t('Statement'));
  $header[] = array('data' => t('Answer'));
  print theme('table', $header, $rows, array(), NULL, TRUE);
}
