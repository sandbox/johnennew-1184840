<?php
/**
 * @file
 * Handles the layout of the multiple_truefalse_question answering form.
 *
 *
 * Variables available:
 * - $form
 */
print drupal_render($form);
