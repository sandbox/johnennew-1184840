<?php
// $Id$

/**
 * @file
 * Defines the classes necessary for a Mutiple True/False quiz question
 *
 * Sponsored by the College of Emergency Medicine http://www.enlightenme.org
 * Code by Deeson Group Ltd http://www.deeson.co.uk
 */

/**
 * Extension of QuizQuestion.
 */
class TrueFalseMultiQuestion extends QuizQuestion {

  /**
   * Implementation of saveNodeProperties
   *
   * @see QuizQuestion#saveNodeProperties($is_new)
   */
  public function saveNodeProperties($is_new = FALSE) {

    if ($is_new || $this->node->revision == 1) {
      $sql = "INSERT INTO {quiz_multiple_truefalse_question_properties} (nid, vid) VALUES (%d, %d)";
      db_query($sql, $this->node->nid, $this->node->vid);
      for ($i = 1; isset($this->node->statements[$i]); $i++) {
        if (drupal_strlen($this->node->statements[$i]['statement']) > 0) {
          $this->insertStatement($i);
        }
      }

    }
    else {

      // We fetch ids for the existing statements belonging to this question
      // We need to figure out if an existing statement has been changed or deleted.
      $sql = 'SELECT id FROM {quiz_multiple_truefalse_question_statement}
              WHERE question_nid = %d AND question_vid = %d';
      $res = db_query($sql, $this->node->nid, $this->node->vid);

      // We start by assuming that all existing alternatives needs to be deleted
      $ids_to_delete = array();
      while ($res_o = db_fetch_object($res)) {
        $ids_to_delete[] = $res_o->id;
      }

      for ($i = 1; isset($this->node->statements[$i]); $i++) {
        $short = $this->node->statements[$i];
        if (drupal_strlen($short['statement']) > 0) {
          // If new statement
          if (!is_numeric($short['id'])) $this->insertStatement($i);
          // If existing statement
          else {
            $this->updateStatement($i);
            // Make sure this alternative isn't deleted
            $key = array_search($short['id'], $ids_to_delete);
            $ids_to_delete[$key] = FALSE;
          }
        }
      }
      foreach ($ids_to_delete as $id_to_delete) {
        if ($id_to_delete) {
          db_query('DELETE FROM {quiz_multiple_truefalse_question_statement} WHERE id = %d', $id_to_delete);
        }
      }
    }
  }

  /**
   * Helper function. Saves new statement
   *
   * @param $i
   *  The statement index
   */
  private function insertStatement($i) {
    $sql = 'INSERT INTO {quiz_multiple_truefalse_question_statement}
            (statement, answer, feedback, question_nid, question_vid)
            VALUES(\'%s\', %d, \'%s\', %d, %d)';
    $short = $this->node->statements[$i];
    db_query($sql, $short['statement'], $short['answer'], $short['feedback'], $this->node->nid, $this->node->vid);
  }

  /**
   * Helper function. Update existing TF statement
   *
   * @param $i
   *  The statement index
   */
  private function updateStatement($i) {
    $sql = 'UPDATE {quiz_multiple_truefalse_question_statement}
      SET statement = \'%s\', answer = %d, feedback = \'%s\'
      WHERE id = %d AND question_nid = %d AND question_vid = %d';
    $short = $this->node->statements[$i];
    db_query($sql, $short['statement'], $short['answer'], $short['feedback'], $short['id'], $this->node->nid, $this->node->vid);
  }

  /**
   * Implementation of validateNode
   *
   * @see QuizQuestion#validateNode($form)
   */
  public function validateNode(array &$form) {
    // This space intentionally left blank. :)
  }

  /**
   * Implementation of delete
   *
   * @see QuizQuestion#delete()
   */
  public function delete($only_this_version = FALSE) {
    $delete_properties = 'DELETE FROM {quiz_multiple_truefalse_question_properties} WHERE nid = %d';
    $delete_targets = 'DELETE FROM {quiz_multiple_truefalse_question_statement} WHERE question_nid = %d';
    $delete_multi_results = '
      DELETE FROM {quiz_multiple_truefalse_question_user_answer_multi}
      USING {quiz_multiple_truefalse_question_user_answer_multi}
      INNER JOIN {quiz_multiple_truefalse_question_user_answers}
      ON {quiz_multiple_truefalse_question_user_answer_multi}.user_answer_id = {quiz_multiple_truefalse_question_user_answers}.id
      WHERE {quiz_multiple_truefalse_question_user_answers}.question_nid = %d'. ($only_this_version ? ' AND question_vid = %d' : '');
    $delete_results = 'DELETE FROM {quiz_multiple_truefalse_question_user_answers} WHERE question_nid = %d';
    if ($only_this_version) {
      $delete_properties .= ' AND vid = %d';
      $delete_targets .= ' AND question_vid = %d';
      $delete_results .= ' AND question_vid = %d';
    }
    db_query($delete_properties, $this->node->nid, $this->node->vid);
    db_query($delete_targets, $this->node->nid, $this->node->vid);
    db_query($delete_multi_results, $this->node->nid, $this->node->vid);
    db_query($delete_results, $this->node->nid, $this->node->vid);

    parent::delete($only_this_version);
  }

  /**
   * Implementation of getNodeProperties
   *
   * @see QuizQuestion#getNodeProperties()
   */
  public function getNodeProperties() {
    if (isset($this->nodeProperties)) return $this->nodeProperties;
    $props = parent::getNodeProperties();

    // Load the statements
    $sql = 'SELECT *
      FROM {quiz_multiple_truefalse_question_statement}
      WHERE question_nid = %d AND question_vid = %d
      ORDER BY id';
    $res = db_query($sql, $this->node->nid, $this->node->vid);
    $props['statements'] = array(); // init array so it can be iterated even if empty

    $i = 1;
    while ($res_arr = db_fetch_array($res)) {
      $props['statements'][$i] = $res_arr;
      $i++;
    }
    $this->nodeProperties = $props;
    return $props;
  }

  /**
   * Implementation of getNodeView
   *
   * @see QuizQuestion#getNodeView()
   */
  public function getNodeView() {
    $content = parent::getNodeView();

    if ($this->viewCanRevealCorrect()) {
      $answer = ($this->node->correct_answer) ? t('True') : t('False');
      $content['answers']['#type'] = 'markup';
      $content['answers']['#value'] = '<div class="quiz-solution">'. $answer .'</div>';
    }
    else {
      $content['answers'] = array(
      '#type' => 'markup',
      '#value' => '<div class="quiz-answer-hidden">Answer hidden</div>',
      '#weight' => 2,
      );
    }
    return $content;
  }

  /**
   * Implementation of getAnsweringForm
   *
   * @see QuizQuestion#getAnsweringForm($form_state, $rid)
   */
  public function getAnsweringForm(array $form_state = NULL, $rid) {
    $form = parent::getAnsweringForm($form_state, $rid);
    $form['#theme'] = 'multiple_truefalse_question_answering_form';

    if (isset($rid)) {
      // This question has already been answered. We load the answer.
      $response = new TrueFalseMultiResponse($rid, $this->node);
      $response = $response->getResponse();
    }

    $form['tries'] = array(
      '#type' => 'fieldset',
      '#title' => t('Answer'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
    );

    $form['tries']['#theme'] = 'multiple_truefalse_question_answer_form_statements';

    for ($i = 1; isset($this->node->statements[$i]); $i++) {
      $short = $this->node->statements[$i];

      $form['tries'][$i]['id'] = array(
        '#type' => 'value',
        '#value' => $short['id'],
      );

      $form['tries'][$i]['user_answer'] = array(
        '#title' => $short['statement'],
        '#type' => 'radios',
        '#options' => array(
          1 => t('True'),
          0 => t('False'),
        ),
        '#default_value' => ( isset($response[$i]['user_answer']) ? $response[$i]['user_answer'] : FALSE ),
        '#required' => FALSE,
        '#weight' => -4,
      );

    }

    return $form;
  }

  /**
   * Implementation of getBodyFieldTitle
   *
   * @see QuizQuestion#getBodyFieldTitle()
   */
  public function getBodyFieldTitle() {
    return t('Instructions or overview');
  }

  /**
   * Implementation of getCreationForm
   *
   * @see QuizQuestion#getCreationForm($form_state)
   */
  public function getCreationForm(array $form_state = NULL) {

    $form['statements'] = array(
      '#type' => 'fieldset',
      '#title' => t('Statements'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#weight' => -4,
      '#tree' => TRUE,
    );

    $form['statements']['#theme'] = 'multiple_truefalse_question_creation';

    for ($i = 1; $i <= 10; $i++) {

      $short = isset($this->node->statements[$i]) ? $this->node->statements[$i] : NULL;

      // We add id to be able to update the correct alternatives if the node is updated, without destroying
      // existing answer reports
      $form['statements'][$i]['id'] = array(
        '#type' => 'value',
        '#value' => $short['id'],
      );

      $form['statements'][$i]['statement'] = array(
        '#type' => 'textfield',
        '#default_value' => isset($short['statement']) ? $short['statement'] : '',
        '#required' => FALSE,
        '#weight' => -4,
      );
      $form['statements'][$i]['answer'] = array(
        '#type' => 'radios',
        '#options' => array(
          1 => t('True'),
          0 => t('False'),
        ),
        '#default_value' => isset($short['answer']) ? $short['answer'] : 1,
        '#required' => FALSE,
        '#weight' => -4,
      );

      $form['statements'][$i]['feedback'] = array(
        '#type' => 'textarea',
        '#required' => FALSE,
        '#default_value' => isset($short['feedback']) ? $short['feedback'] : '',
      );
    }

    return $form;
  }

  /**
   * Implementation of getMaximumScore
   *
   * @see QuizQuestion#getMaximumScore()
   */
  public function getMaximumScore() {
    $max = 0;
    for ($i = 1; isset($this->node->statements[$i]); $i++) {
      if (drupal_strlen($this->node->statements[$i]['statement'])>0) {
        $max++;
      }
    }
    return max($max, 1);
  }

}

/**
 * Extension of QuizQuestionResponse
 */
class TrueFalseMultiResponse extends QuizQuestionResponse {

  protected $user_answers;

  /**
   * Constructor
   */
  public function __construct($result_id, stdClass $question_node, $tries = NULL) {
    parent::__construct($result_id, $question_node, $tries);
    $this->user_answers = array();

    if (is_array($tries)) {
      $responses = isset($tries['answer_id']) ? $tries['answer'] : $tries;
      foreach ($responses as $id => $response) {
        $this->user_answers[$id]['user_answer'] = $response['user_answer'];
        $this->user_answers[$id]['statement_id'] = $question_node->statements[$id]['id'];
      }
    }
    // We load the answer from the database
    else {
      $sql = 'SELECT statement_id, user_answer
        FROM {quiz_multiple_truefalse_question_user_answers} ua
        LEFT OUTER JOIN {quiz_multiple_truefalse_question_user_answer_multi} uam ON (uam.user_answer_id = ua.id)
        WHERE ua.result_id = %d AND ua.question_nid = %d AND ua.question_vid = %d';
      $res = db_query($sql, $result_id, $this->question->nid, $this->question->vid);
      $i = 1;
      while ($res_o = db_fetch_array($res)) {
        $this->user_answers[$i] = $res_o;
        $i++;
      }
    }
  }

  /**
   * Implementation of isValid
   *
   * @see QuizQuestionResponse#isValid()
   */
  public function isValid() {
    $ok = TRUE;
    foreach ($this->user_answers as $id => $answer) {
      if ($answer === NULL) $ok = FALSE;
    }

    if ($ok) return TRUE;
    return t('You must provide an answer to every statement');
  }

  /**
   * Implementation of save
   *
   * @see QuizQuestionResponse#save()
   */
  public function save() {
    $sql = "INSERT INTO {quiz_multiple_truefalse_question_user_answers} (question_nid, question_vid, result_id) VALUES (%d, %d, %d)";
    db_query($sql, $this->question->nid, $this->question->vid, $this->rid, (int)$this->answer, (int)$this->getScore());
    $user_answer_id = db_last_insert_id('{quiz_multiple_truefalse_question_user_answers}', 'id');
    for ($i = 1; $i <= count($this->user_answers); $i++) {
      $sql = 'INSERT INTO {quiz_multiple_truefalse_question_user_answer_multi}
      (user_answer_id, statement_id, user_answer)
      VALUES(%d, %d, %d)';
      db_query($sql, $user_answer_id, $this->user_answers[$i]['statement_id'], $this->user_answers[$i]['user_answer']);
    }
  }

  /**
   * Implementation of delete
   *
   * @see QuizQuestionResponse#delete()
   */
  public function delete() {
    $sql = 'DELETE FROM {quiz_multiple_truefalse_question_user_answer_multi}
    USING {quiz_multiple_truefalse_question_user_answer_multi}
    INNER JOIN {quiz_multiple_truefalse_question_user_answers}
    ON {quiz_multiple_truefalse_question_user_answer_multi}.user_answer_id = {quiz_multiple_truefalse_question_user_answers}.id
    WHERE {quiz_multiple_truefalse_question_user_answers}.question_nid = %d
    AND {quiz_multiple_truefalse_question_user_answers}.question_vid = %d
    AND {quiz_multiple_truefalse_question_user_answers}.result_id = %d';
    db_query($sql, $this->question->nid, $this->question->vid, $this->rid);
    $sql = 'DELETE FROM {quiz_multiple_truefalse_question_user_answers}
    WHERE result_id = %d AND question_nid = %d AND question_vid = %d';
    db_query($sql, $this->rid, $this->question->nid, $this->question->vid);
  }

  /**
   * Implementation of score
   *
   * @see QuizQuestionResponse#score()
   */
  public function score() {
    $score = 0;
    foreach ($this->question->statements as $key => $statement) {
      if ($statement['answer']==$this->user_answers[$key]['user_answer']) {
        $score ++;
      }
    }
    return $score;
  }

  /**
   * Implementation of getResponse
   *
   * @see QuizQuestionResponse#getResponse()
   */
  public function getResponse() {
    return $this->user_answers;
  }

  /**
   * Implementation of getReportFormResponse
   *
   * @see QuizQuestionResponse#getReportFormResponse($showpoints, $showfeedback, $allow_scoring)
   */
  public function getReportFormResponse($showpoints = TRUE, $showfeedback = TRUE, $allow_scoring = FALSE) {
    if (empty($this->question->statements)) {
      return array(
        '#type' => 'markup',
        '#value' => t('No statements have been set for this multiple True False question.'),
      );
    }
    $metadata = array();
    $data = array();

    $metadata['showpoints'] = $showpoints;
    $metadata['showfeedback'] = $showfeedback;

    $i = 1;
    while (isset($this->question->statements[$i])) {
      $short = $this->question->statements[$i];
      if (drupal_strlen(check_plain($short['statement'])) > 0) {
        $data[$i]['statement'] = check_plain($short['statement']);
        $data[$i]['correct_answer'] = $short['answer'];
        $data[$i]['user_answer'] = $this->user_answers[$i]['user_answer'];
        $data[$i]['correct'] = $data[$i]['correct_answer'] == $data[$i]['user_answer'];
        $data[$i]['feedback'] = check_plain($short['feedback']);
      }
      $i++;
    }

    // Return themed output
    return array(
      '#type' => 'markup',
      '#value' => theme('multiple_truefalse_question_response', $metadata, $data),
    );
  }
}
